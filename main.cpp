#include <SFML/Graphics.hpp>
#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cmath>
#include <thread>
#include <unistd.h>

#define PORT 44493
#define PIc 3.14159265
#define IP "192.168.0.108"

using namespace std;
bool flag = true;
bool game_started = 1;
char gl_recv[20];

int HeroNumber;

string XS, YS;
string global;

sf::Texture texture;
sf::Texture dead;



class cPlayer
{
public:
    cPlayer()
    {

        this->Player.setTexture(texture);
        this->Player.setScale(0.75, 0.75);
        this->Player.setOrigin(sf::Vector2f((Player.getLocalBounds().width / 2) - 40, (Player.getLocalBounds().height / 2) + 30));
        this->hpbar.setOrigin(sf::Vector2f((Player.getLocalBounds().width / 2) - 40, (Player.getLocalBounds().height / 2) + 30));
        this->hpbar.setSize(sf::Vector2f(health, 5));
        this->hpbar.setFillColor(sf::Color::Green);
        /*	if(!this->font.loadFromFile("f.ttf")<0)
                cout<<"font error\n";
            this->health_indicator.setFont(font);
            this->health_indicator.setPosition(10, 10);
            this->health_indicator.setCharacterSize(15);
            this->health_indicator.setStyle(sf::Text::Bold);
            this->health_indicator.setString("ban");
            this->health_indicator.setFillColor(sf::Color::White);*/
    }
    void setpos(int x, int y)
    {
        this->Player.setPosition(x, y);
    }
    void setrot(double r)
    {
        this->Player.setRotation(r);
    }
    void draw(sf::RenderWindow &window)
    {
        window.draw(this->Player);
        //window.draw(this->health_indicator);
        window.draw(this->hpbar);
    }
    sf::Vector2f getpos()
    {
        return this->Player.getPosition();
    }
    double getrot()
    {
        return this->Player.getRotation();
    }
    void setxy(int a, int b)
    {
        x = a;
        y = b;
    }
    int getx()
    {
        return x;
    }
    int gety()
    {
        return y;
    }
    void damage()
    {
        health -= 35;
    }
    int getHealth()
    {
        return health;
    }
    void updIndicator()
    {
        health_indicator.setString(to_string(health));
        health_indicator.setPosition(x, y);
        hpbar.setPosition(x, y);
        hpbar.setSize(sf::Vector2f(health, 5));
    }
    int hitBoxSize = 60;
    void die()
    {
        active = false;
        Player.setTexture(dead);
        Player.setRotation(0);
    }
    bool is_active()
    {
        return active;
    }
private:
    bool active = true;
    int health = 100;
    int x, y;
    sf::Sprite Player;
    sf::Text health_indicator;
    sf::Font font;
    sf::RectangleShape hpbar;
};

vector <cPlayer> all_players;

class Bullet
{
public:
    Bullet(sf::Vector2f a,double rot, int owner)
    {
        OwnerNumber = owner;
        bullet.setOrigin(width / 2-120, height / 2-15);
        bullet.setPosition(a);
        bullet.setFillColor(sf::Color::Yellow);
        bullet.setSize(sf::Vector2f(width, height));

        bullet.setRotation(rot);
        speedX = mult*cos((360-rot)*PIc / 180);
        speedY = -mult*sin((360-rot)*PIc / 180);

    }
    void draw(sf::RenderWindow &window)
    {
        window.draw(bullet);
    }
    void motion()
    {
        bullet.move(speedX, speedY);
    }
    bool did_hit(int hitbox, sf::Vector2f victim_pos)
    {

        return sqrt(pow(bullet.getPosition().x - victim_pos.x, 2) + pow(bullet.getPosition().y - victim_pos.y, 2)) < hitbox;

    }
    int getOwnerNumber()
    {
        return OwnerNumber;
    }
private:
    bool active = true;
    int width = 60;
    int height = 5;
    int OwnerNumber;
    sf::RectangleShape bullet;
    int mult=25,speedX, speedY;

};
vector<Bullet> bullets;

void rotate_to_mouse(sf::RenderWindow &win, sf::RectangleShape &rect)
{
    sf::Vector2f currentPos = rect.getPosition();
    sf::Vector2i mousePos = sf::Mouse::getPosition(win);

    const double PI = 3.14159265;
    double dx = currentPos.x - mousePos.x;
    double dy = currentPos.y - mousePos.y;
    double rotation = (atan2(dy, dx)) * 180 / PI;
    rect.setRotation(rotation);
}
void recv2(int s1)
{
    string temp1, temp2;
    bool my_con = true;
    while (flag) {
        char receive_char[32];
        //memset(receive_char, 0, 19);
        int result_rec = recv(s1,receive_char,32, 0);
        string str = receive_char;
        temp2 = str;
        global = str;
        bool flag = 0;


        if (result_rec < 0)
        {
            puts("Receive failed\n");
            flag = 0;

        }
        else if (result_rec == 0)
        {
            cout << "\n\nServer closed...\n\n";
            flag = 0;
        }
        else
        {
            //cout<<"Data received:\n";
            //std::cout << receive_char<< std::endl;
            if (temp2.substr(0,3) == "new")
            {
                cout << "check\n";
                if (my_con)
                {
                    my_con=!my_con;
                    int old_players_number = stoi(temp2.substr(4));
                    for (int i = 0; i < old_players_number; i++)
                    {
                        cPlayer p;
                        all_players.push_back(p);
                    }
                    //cout << "Players on server: " << all_players.size() << endl;
                }
                else
                {
                    cPlayer p;
                    all_players.push_back(p);
                }

            }
            else if (temp2.substr(0, 3) == "!@#")
            {
                HeroNumber = stoi(temp2.substr(3));
                //cout << "My number is " << HeroNumber << endl;
            }
            else if (temp2[0] == 'd')
            {
                cout << "stoi:" << temp2.substr(2, temp2.find_first_of("#")) << "!";
                int villainNumber = stoi(temp2.substr(2, temp2.find_first_of("#")));
                all_players[villainNumber].damage();
                if (all_players[villainNumber].getHealth() <= 0)
                {
                    all_players[villainNumber].die();
                    cout << villainNumber << " is dead\n";
                }
                cout << villainNumber << "is now " << all_players[villainNumber].getHealth() << endl;

            }
                //else if (temp2[0] == 'b')
                //{
                //	temp2 = temp2.substr(3);
                //	//cout << temp2<<endl;
                //	int space = temp2.find_first_of(" ");
                //	int ownerNumber = stoi(temp2.substr(0, space));
                //	temp2 = temp2.substr(space + 1);
                //	space = temp2.find_first_of(" ");
                //	int x = stoi(temp2.substr(0, space));
                //	temp2 = temp2.substr(space + 1);
                //	space = temp2.find_first_of(" ");
                //	int y = stoi(temp2.substr(0, space));
                //	temp2 = temp2.substr(space + 1);
                //	double rot = stod(temp2.substr(0, temp2.find_first_of("#")));
                //	sf::Vector2f coords(x, y);
                //	Bullet b(coords, rot,ownerNumber);
                //	bullets.push_back(b);



                //}
            else
            {
                for (int i=0;i<all_players.size();i++)
                {
                    if(temp2.size()!=0)
                    {
                        string temp_individual = temp2.substr(0, temp2.find_first_of("#"));
                        temp2 = temp2.substr(temp2.find_first_of("#") + 1);
                        //cout << "temp2:" << temp2 << "size:" <<temp2.size()<<"end"<< endl;
                        int space = temp_individual.find_first_of(" ");
                        //cout << "X: " << temp_individual.substr(0, space);
                        int x = stoi(temp_individual.substr(0, space));
                        temp_individual = temp_individual.substr(space + 1, temp_individual.find_first_of("#"));
                        //cout << " y: " << temp_individual << "end" << endl;
                        int y = stoi(temp_individual);
                        if (i != HeroNumber)
                        {
                            //all_players[i].setxy(x, y);
                            all_players[i].setpos(x, y);
                        }
                    }


                }
                game_started = 1;
            }

        }
        //this_thread::sleep_for(50ms);
    }

}




int windowWidth = 800, windowHeight = 800;
int dist = 3;
bool wp = 0, ap = 0, sp = 0, dp = 0;
double fovAngle = 20;
string path_to_player_texture = "../player.png";
string path_to_dead = "../dead.png";



vector <sf::Sprite> players;


int main()
{

    int x = 400, y = 400;
    texture.loadFromFile(path_to_player_texture);
    dead.loadFromFile(path_to_dead);

    //Creatin socket _---------------------------------------------------------------------
    int s = socket (AF_INET, SOCK_STREAM,0);
    //Socket created------------------------------------------------------------------------

    struct sockaddr_in server;
    server.sin_addr.s_addr = inet_addr(IP);//óêàçûâàåì IPv4-àäðåñ ñåðâåðà 172.20.10.4
    server.sin_family = AF_INET;//ïðîòîêîë
    server.sin_port = htons(PORT);//ïîðò



    int con_status;
    do
    {
        con_status = connect(s, (struct sockaddr *)&server, sizeof(server));
    } while (con_status < 0);

    std::string msg_str;

    const char* msg;
    msg_str = "";
    msg_str.append(std::to_string(x));
    msg_str.append(" ");
    msg_str.append(std::to_string(y));
    msg = msg_str.c_str();
    msg = msg_str.c_str();
    int dataSize = sizeof(msg) / sizeof(char);

    thread recv_thread(recv2,s);
    recv_thread.detach();
    //recv_thread.join();



    //SFML part----------------------------------------------
    {




        sf::ContextSettings settings;
        settings.antialiasingLevel = 16;

        sf::RectangleShape zanaves(sf::Vector2f(windowWidth, windowHeight));
        zanaves.setFillColor(sf::Color::Green);


        sf::RenderWindow window(sf::VideoMode(windowWidth, windowHeight), "SFML works!", sf::Style::Default, settings);
        sf::RectangleShape rectangle;
        int rect_size = 5;
        rectangle.setFillColor(sf::Color::Yellow);
        rectangle.setSize(sf::Vector2f(rect_size, rect_size));
        rectangle.setOrigin(rect_size / 2,rect_size/2);



        sf::RectangleShape background(sf::Vector2f(windowWidth, windowHeight));
        background.setFillColor(sf::Color(50, 50, 50));

        int correction = 30;
        int fov_rect_size = 5000;
        sf::RectangleShape fov1(sf::Vector2f(fov_rect_size,fov_rect_size));
        fov1.setOrigin(fov_rect_size+correction, fov_rect_size/2);
        fov1.setFillColor(sf::Color::Black);

        sf::RectangleShape fov2(fov1);
        sf::RectangleShape fov3(fov1);
        fov3.setOrigin(fov_rect_size / 2, fov_rect_size);
        fov2.setOrigin(0-correction, fov_rect_size/2);




        sf::Font font;
        if (!font.loadFromFile("../f.ttf"))
        {
            cout << "error\n";
        }
        sf::Text text;
        text.setFont(font);
        text.setPosition(10, 10);
        text.setCharacterSize(30);
        text.setStyle(sf::Text::Bold);
        text.setFillColor(sf::Color::Red);

        sf::Text server_text1;
        server_text1.setFont(font);
        server_text1.setPosition(10, 50);
        server_text1.setCharacterSize(30);
        server_text1.setStyle(sf::Text::Bold);
        server_text1.setFillColor(sf::Color::Blue);




        while (window.isOpen())
        {
            sf::Event event;
            while (window.pollEvent(event))
            {

                rotate_to_mouse(window, rectangle);
                rotate_to_mouse(window, fov1);
                rotate_to_mouse(window, fov2);
                rotate_to_mouse(window, fov3);
                if(all_players.size()!=0&& all_players[HeroNumber].is_active())
                    all_players[HeroNumber].setrot(rectangle.getRotation()+180);

                fov1.rotate(-fovAngle-90);
                fov2.rotate(fovAngle-90);
                fov3.rotate(90);

                if (event.type == sf::Event::Closed)
                {
                    window.close();
                    flag = false;
                }


                if ((event.type == sf::Event::MouseButtonPressed) && (event.mouseButton.button == sf::Mouse::Button::Left)&&all_players[HeroNumber].is_active())
                {
                    std::cout << "bang!\n";
                    //Bullet my_bullet(all_players[HeroNumber].getpos(), all_players[HeroNumber].getrot());
                    /*msg_str = "b ";
                    msg_str.append(std::to_string(HeroNumber));
                    msg_str.append(" ");
                    msg_str.append(std::to_string(x));
                    msg_str.append(" ");
                    msg_str.append(std::to_string(y));
                    msg_str.append(" ");
                    msg_str.append(std::to_string(all_players[HeroNumber].getrot()));
                    msg = msg_str.c_str();*/
                    for (int k = 0; k < all_players.size(); k++)
                    {

                        if ((event.mouseButton.x < all_players[k].getpos().x + all_players[k].hitBoxSize) && (event.mouseButton.x > all_players[k].getpos().x - all_players[k].hitBoxSize) && (event.mouseButton.y < all_players[k].getpos().y + all_players[k].hitBoxSize) && (event.mouseButton.y > all_players[k].getpos().y - all_players[k].hitBoxSize))
                        {
                            cout << "hit " << k << " !\n";
                            msg_str = "";
                            msg_str.append("d ");
                            msg_str.append(to_string(k));
                            msg = msg_str.c_str();
                            int send_res;
                            if ((send_res = send(s, msg, sizeof(msg) / sizeof(char), 0)) < 0)
                            {
                                cout << send_res << " Send error! While sending:" << msg << "\n\n";
                            }
                            cout << "Data send: " << msg << "send status=" << send_res << endl;
                        }
                    }


                    //bullets.push_back(my_bullet);


                }

                if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::W))wp = 1;
                if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::S)) sp = 1;
                if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::A)) ap = 1;
                if ((event.type == sf::Event::KeyPressed) && (event.key.code == sf::Keyboard::D)) dp = 1;
                if ((event.type == sf::Event::KeyReleased) && (event.key.code == sf::Keyboard::W))wp = 0;
                if ((event.type == sf::Event::KeyReleased) && (event.key.code == sf::Keyboard::S)) sp = 0;
                if ((event.type == sf::Event::KeyReleased) && (event.key.code == sf::Keyboard::A)) ap = 0;
                if ((event.type == sf::Event::KeyReleased) && (event.key.code == sf::Keyboard::D)) dp = 0;





            }






            if(all_players[HeroNumber].is_active())
            {
                if (wp) y -= dist;
                if (sp) y += dist;
                if (ap) x -= dist;
                if (dp) x += dist;
            }

            //Correcting x and y
            {
                if (y < 0) y = 0;
                if (y + 2 > windowHeight) y = windowHeight;
                if (x < 0) x = 0;
                if (x + 2 > windowWidth) x = windowWidth;
            }
            //------------------
            //Forming message
            {



                msg_str = "";
                msg_str.append(std::to_string(x));
                msg_str.append(" ");
                msg_str.append(std::to_string(y));
               // msg_str.append(" ");
                //msg_str.append(std::to_string(all_players[HeroNumber].getrot()));
                msg = msg_str.c_str();
            }
            //--------------
            if ((wp || sp || ap || dp)&&(flag) && all_players[HeroNumber].is_active())
            {
                int send_res;
                if ((send_res=send(s, msg, sizeof(msg) / sizeof(char), 0)) < 0)
                {
                    cout << send_res << " Send error! While sending:"<<msg<<"\n\n";
                }
                cout <<"Data send: "<< msg <<"send status="<<send_res<< endl;
            }
            string coordsShow = "";
            coordsShow.append(to_string(x));
            coordsShow.append(" ");
            coordsShow.append(to_string(y));
            text.setString(coordsShow);
            string server_text = "";
            server_text.append(XS);
            server_text.append(" ");
            server_text.append(YS);
            server_text1.setString(global);




            rectangle.setPosition(x, y);
            if (all_players.size() != 0)
                all_players[HeroNumber].setpos(x, y);


            fov1.setPosition(x, y);
            fov2.setPosition(x, y);
            fov3.setPosition(x, y);



            window.clear();
            window.draw(background);
            /*for (int i = 0; i < bullets.size(); i++)
            {
                bullets[i].motion();

            }*/
            //for (auto b : bullets)
            //{
            //	b.draw(window);
            ///*	for (int j = 0; j < all_players.size(); j++)
            //	{
            //		if (b.did_hit(all_players[j].hitBoxSize, all_players[j].getpos()))
            //		{
            //			cout << b.getOwnerNumber() << " hit " << j << endl;
            //			all_players[j].damage();
            //			if (all_players[j].getHealth() < 0)
            //			{
            //				cout << b.getOwnerNumber() << " killed " << j << endl;
            //				all_players[j].updIndicator();
            //			}
            //		}
            //	}*/
            //}
            for (int i = 0; i < all_players.size(); i++)
            {
                if (i != HeroNumber)
                {
                    all_players[i].updIndicator();
                    all_players[i].draw(window);
                }
            }
            if (all_players[HeroNumber].is_active()){}
            /*{
                window.draw(fov1);
                window.draw(fov2);
                window.draw(fov3);
            }*/

            all_players[HeroNumber].draw(window);
            window.draw(rectangle);
            window.draw(text);
            window.draw(server_text1);
            if (!game_started)
            {
                window.draw(zanaves);
            }
            window.display();
        }
    }
    //end of SFML part --------------------------------------


    close(s);
    return 0;
}